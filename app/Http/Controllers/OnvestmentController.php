<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Onvestment;
use App\Customer;

class OnvestmentController extends Controller
{
    public function index()
    {
        //
        $onvestments=Onvestment::all();
        return view('onvestments.index',compact('onvestments'));
    }

    public function show($id)
    {
        
        $onvestment = Onvestment::findOrFail($id);

        return view('onvestments.show',compact('onvestment'));
    }


    public function create()
    {

        $customers = Customer::lists('name','id');
        return view('onvestments.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)   
    {

       $onvestment= new Onvestment($request->all());
       $onvestment->save();

              return redirect('onvestments');
    }

    public function edit($id)
    {
        $onvestment=Onvestment::find($id);
        return view('onvestments.edit',compact('onvestment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        //
        $onvestment= new Onvestment($request->all());
        $onvestment=Onvestment::find($id);
        $onvestment->update($request->all());
        return redirect('onvestments');
    }

    public function destroy($id)
    {
        Onvestment::find($id)->delete();
        return redirect('onvestments');
    }
}


