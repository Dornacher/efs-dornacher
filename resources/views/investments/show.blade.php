@extends('app')
@section('content')
	<br>
    <h1>Investment </h1>
	<br>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="bg-info">
			<tr>
                <td>Customer Number</td>
                <td><?php echo ($investment->customer->cust_number); ?></td>
            </tr>
			<tr>
                <td>Customer Name</td>
                <td><?php echo ($investment->customer->name); ?></td>
            </tr>
			
            <tr>
                <td>Investment Category</td>
                <td><?php echo ($investment['category']); ?></td>
            </tr>
            <tr>
                <td>Investment Description</td>
                <td><?php echo ($investment['description']); ?></td>
            </tr>
            <tr>
                <td>Aquired Value</td>
                <td><?php echo ($investment['acquired_value']); ?></td>
            </tr>
            <tr>
                <td>Aquired Date</td>
                <td><?php echo ($investment['acquired_date']); ?></td>
            </tr>
            <tr>
                <td>Recent Value</td>
                <td><?php echo ($investment['recent_value']); ?></td>
            </tr>
			<tr>
                <td>Recent Date</td>
                <td><?php echo ($investment['recent_date']); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
