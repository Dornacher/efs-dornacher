@extends('app')
@section('content')
	<br>
    <h1>Update Investement</h1>
    {!! Form::model($investment,['method' => 'PATCH','route'=>['investments.update',$investment->id]]) !!}
	<br><br>
    <div class="form-group">
        {!! Form::label('category', 'Investment Category:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('category',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('description', 'Investment Description:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('description',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('acquired_value', 'Aquired Value:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('acquired_value',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('acquired_date', 'Aquired Date:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('acquired_date',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('recent_value', 'Recent Value:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('recent_value',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
	<div class="form-group">
        {!! Form::label('recent_date', 'Recent Date:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('recent_date',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop
