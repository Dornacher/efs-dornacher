@extends('app')
@section('content')
	<br>
    <h1>Update Stock</h1>
    {!! Form::model($stock,['method' => 'PATCH','route'=>['stocks.update',$stock->id]]) !!}
	<br><br>
       <div class="form-group">
        {!! Form::label('symbol', 'Symbol:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('symbol',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('name', 'St Name:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('name',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('shares', 'Shares:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('shares',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('purchase_price', 'Purchase Price:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('purchase_price',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('purchased', 'Purchase Date:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('purchased',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop
