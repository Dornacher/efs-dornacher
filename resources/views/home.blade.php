@extends('app')

@section('content')
<div class="container">
	<div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to Eagle Financial Services</div>

                <div class="panel-body">
                    Eagle Financial Services is a comprehensive regional financial services firm, 
					represented by some of the most knowledgeable and successful financial professionals. 
					With over 300 years of cumulative experience, our team includes an investment specialist,
					a group benefit division, small business counselors, and specialists in Estate Planning. 
					Our commitment to outstanding service is evident in all our available resources: our quality staff, 
					competitive products, and highly knowledgeable associates. We focus on making our clients comfortable 
					in the knowledge that their financial security is our number one priority.
					 <?php echo '<p></p>'; ?>
					 Please explore our different services that we offer:
					<?php echo '<p></p>'; ?>
					<a href="{{ action('CustomerController@index') }}">Customers</a> |
					<a href="{{ action('StockController@index') }}">Stocks</a> |
					<a href="{{ action('InvestmentController@index') }}">Investments</a> |
					<a href="{{ action('OnvestmentController@index') }}">Mutual Funds</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
