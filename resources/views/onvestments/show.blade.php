@extends('app')
@section('content')
	<br>
    <h1>Mutual Fund </h1>
	<br>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="bg-info">
			<tr>
                <td>Customer Number</td>
                <td><?php echo ($onvestment->customer->cust_number); ?></td>
            </tr>
			<tr>
                <td>Customer Name</td>
                <td><?php echo ($onvestment->customer->name); ?></td>
            </tr>
			
            <tr>
                <td>Fund Category</td>
                <td><?php echo ($onvestment['category']); ?></td>
            </tr>
            <tr>
                <td>Fund Description</td>
                <td><?php echo ($onvestment['description']); ?></td>
            </tr>
            <tr>
                <td>Aquired Value</td>
                <td><?php echo ($onvestment['acquired_value']); ?></td>
            </tr>
            <tr>
                <td>Aquired Date</td>
                <td><?php echo ($onvestment['acquired_date']); ?></td>
            </tr>
            <tr>
                <td>Recent Value</td>
                <td><?php echo ($onvestment['recent_value']); ?></td>
            </tr>
			<tr>
                <td>Recent Date</td>
                <td><?php echo ($onvestment['recent_date']); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
