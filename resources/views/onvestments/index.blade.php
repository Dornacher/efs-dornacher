@extends('app')

@section('content')
    <h1>Mutual Funds</h1>
    <a href="{{url('/onvestments/create')}}" class="btn btn-success">Create Mutual Fund</a>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="bg-info">
			<th></th>
            <th>Cust No</th>
            <th>Cust Name</th>
            <th>Category</th>
            <th>Description</th>
            <th>Aquired Value</th>
            <th>Aquired Date</th>
            <th>Recent Value</th>
			<th>Recent Date</th>
            <th colspan="3">Actions</th>
			<th></th>
        
		
		</tr>
        </thead>
        <tbody>
        @foreach ($onvestments as $onvestment)
            <tr>
				<th></th>
                <td>{{ $onvestment->customer->cust_number }}</td>
                <td>{{ $onvestment->customer->name }}</td>
                <td>{{ $onvestment->category }}</td>
                <td>{{ $onvestment->description }}</td>
                <td>{{ $onvestment->acquired_value }}</td>
                <td>{{ $onvestment->acquired_date }}</td>
                <td>{{ $onvestment->recent_value }}</td>
				<td>{{ $onvestment->recent_date }}</td>
				
				
                <td><a href="{{url('onvestments',$onvestment->id)}}" class="btn btn-primary">Read</a></td>
                <td><a href="{{route('onvestments.edit',$onvestment->id)}}" class="btn btn-warning">Update</a></td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'route'=>['onvestments.destroy', $onvestment->id]]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
				<th></th>
            </tr>
        @endforeach

        </tbody>

    </table>
@endsection

