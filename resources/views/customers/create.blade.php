@extends('app')
@section('content')
    <h1>Create New Customer</h1>
    {!! Form::open(['url' => 'customers']) !!}
    <br>
    <div class="form-group">
        {!! Form::label('name', 'Name:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('name',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('cust_number', 'Cust Number',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('cust_number',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('address', 'Street Address:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('address',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('city', 'City:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('city',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('state', 'State:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('state',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('zip', 'Zip:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('zip',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('email', 'Primary Email:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('email',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('home_phone', 'Home Phone:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('home_phone',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::label('cell_phone', 'Cell Phone:',['class'=>'col-md-3 col-md-offset-2']) !!}
        {!! Form::text('cell_phone',null,['class'=>'col-md-3']) !!}
    </div>
	<br><br>
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop

