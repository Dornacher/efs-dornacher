@extends('app')
@section('content')
	<br>
    <h1>Customer </h1>
	<br>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="bg-info">
            <tr>
                <td>Name</td>
                <td><?php echo ($customer['name']); ?></td>
            </tr>
            <tr>
                <td>Cust Number</td>
                <td><?php echo ($customer['cust_number']); ?></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><?php echo ($customer['address']); ?></td>
            </tr>
            <tr>
                <td>City </td>
                <td><?php echo ($customer['city']); ?></td>
            </tr>
            <tr>
                <td>State</td>
                <td><?php echo ($customer['state']); ?></td>
            </tr>
            <tr>
                <td>Zip </td>
                <td><?php echo ($customer['zip']); ?></td>
            </tr>
            <tr>
                <td>Home Phone</td>
                <td><?php echo ($customer['home_phone']); ?></td>
            </tr>
            <tr>
                <td>Cell Phone</td>
                <td><?php echo ($customer['cell_phone']); ?></td>
            </tr>
            </tbody>
      </table>
    </div>
	
    <?php
    $stockprice=null;
	$totalStockPrice = 0;
	$initialInvestmentPrice = 0;
	$currentInvestmentPrice = 0;
	$initialOnvestmentPrice = 0;
	$currentOnvestmentPrice = 0;
    $stotal = 0;
    $svalue=0;
    $itotal = 0;
    $ivalue=0;
    $iportfolio = 0;
    $cportfolio = 0;
    ?>
	
	
		
		
    <br>
    <h2>Stocks </h2>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Symbol </th>
                <th>Stock Name</th>
                <th>No. of Shares</th>
                <th>Purchase Price</th>
                <th>Purchase Date</th>
				<th>Initial Value</th>
            </tr>
            </thead>

            <tbody>




				@foreach($customer->stocks as $stock)
                <tr>
                <td>{{ $stock->symbol }}</td>
                <td>{{ $stock->name }}</td>
				<td>{{ $stock->shares }}</td>
				<td>{{ $stock->purchase_price }}</td>
				<td>{{ $stock->purchased }}</td>
				<td>{{ $stock->shares*$stock->purchase_price }}</td>

				<?php $totalStockPrice = $totalStockPrice + $stock->shares*$stock->purchase_price; ?>
                </tr>
				@endforeach
	
			</tbody>
		</table>

		Total Stock Portfolio Value  = $ {{$totalStockPrice}}

    </div>
		
	
	
    <br>
    <h2>Investments </h2>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Category </th>
                <th>Description</th>
                <th>Aquired Value</th>
                <th>Aquired Date</th>
                <th>Recent Value</th>
                <th>Recent Date</th>
                
            </tr>
            </thead>

            <tbody>
				@foreach($customer->investments as $investment)
                <tr>
                <td>{{ $investment->category }}</td>
                <td>{{ $investment->description }}</td>
				<td>{{ $investment->acquired_value }}</td>
				<td>{{ $investment->acquired_date }}</td>
				<td>{{ $investment->recent_value }}</td>
				<td>{{ $investment->recent_date }}</td>
				<?php $initialInvestmentPrice = $initialInvestmentPrice + $investment->acquired_value; ?>
				<br>
				<?php $currentInvestmentPrice = $currentInvestmentPrice + $investment->recent_value; ?>
                </tr>
				@endforeach
			</tbody>
		</table>

		Total of Initial Investment Portfolio  = $ {{$initialInvestmentPrice}}
		<?php echo '<p></p>'; ?>
		Total of Current Investment Portfolio  = $ {{$currentInvestmentPrice}}	
    </div>	
	
		
	
    <br>
    <h2>Mutual Funds </h2>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Category </th>
                <th>Description</th>
                <th>Aquired Value</th>
                <th>Aquired Date</th>
                <th>Recent Value</th>
                <th>Recent Date</th>
                
            </tr>
            </thead>

            <tbody>
				@foreach($customer->onvestments as $onvestment)
                <tr>
                <td>{{ $onvestment->category }}</td>
                <td>{{ $onvestment->description }}</td>
				<td>{{ $onvestment->acquired_value }}</td>
				<td>{{ $onvestment->acquired_date }}</td>
				<td>{{ $onvestment->recent_value }}</td>
				<td>{{ $onvestment->recent_date }}</td>
				<?php $initialOnvestmentPrice = $initialOnvestmentPrice + $onvestment->acquired_value; ?>
				<br>
				<?php $currentOnvestmentPrice = $currentOnvestmentPrice + $onvestment->recent_value; ?>
                </tr>
				@endforeach
			</tbody>
		</table>

		Total of Initial Mutual Funds Portfolio  = $ {{$initialOnvestmentPrice}}
		<?php echo '<p></p>'; ?>
		Total of Current Mutual Funds Portfolio  = $ {{$currentOnvestmentPrice}}	
    </div>	

	  <br>
    <h2>Summary of Portfolio </h2>
    <div class="container">


		Total of Initial Portfolio Value = $ {{$totalStockPrice+$initialInvestmentPrice+$initialOnvestmentPrice}}
		<?php echo '<p></p>'; ?>
		Total of Current Portfolio Value  = $ {{$totalStockPrice+$currentInvestmentPrice+$currentOnvestmentPrice}}	
    </div>	
	
@stop
	
	
	
	


